<form action="{{ $action}}" method="POST" enctype="multipart/form-data">
    @csrf
    <!-- .form-group>label{title}+input.form-control[name=title] -->

        <div class="form-group"><label for="">Name</label>
        <input type="text" class="form-control {{ $errors->has('name')? 'is-invalid': ''}}" name="name" value="{{  isset($companies) ? $companies->name : '' }}">
        @if($errors->has('name'))
        <div class="invalid-feedback">{{ $errors->first('name')}}</div>
        @endif
        </div>

        <div class="form-group"><label for="">Email</label>
        <input type="email" class="form-control {{ $errors->has('email')? 'is-invalid': ''}}" name="email" value="{{  isset($companies) ? $companies->email : '' }}">
        @if($errors->has('email'))
        <div class="invalid-feedback">{{ $errors->first('email')}}</div>
        @endif
        </div>

        <div class="form-group"><label for="">Website</label>
        <input type="text" class="form-control {{ $errors->has('website')? 'is-invalid': ''}}" name="website" value="{{  isset($companies) ? $companies->website : '' }}">
        @if($errors->has('website'))
        <div class="invalid-feedback">{{ $errors->first('website')}}</div>
        @endif
        </div>

        <!-- .form-group>label{Body}+textarea.form-control[name=body] -->

        <div class="form-group"><label for="">Address</label>
        <input type="text" class="form-control {{ $errors->has('address')? 'is-invalid': ''}}" name="address" value="{{  isset($companies) ? $companies->address : '' }}">
        @if($errors->has('website'))
        <div class="invalid-feedback">{{ $errors->first('website')}}</div>
        @endif
        </div>


        <div class="form-group"><label for="">Logo</label>
            <input type="file" name="logo" class="form-control{{ $errors->has('logo') ? ' is-invalid' : '' }}" >
            @if ($errors->has('logo'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('logo') }}</strong>
                </span>
            @endif
        </div>

        <!-- button.btn.btn-primary{submit} -->

        <button class="btn btn-primary">submit</button>

    </form>
