<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Employees;
use App\Companies;
use Faker\Generator as Faker;

$factory->define(Employees::class, function (Faker $faker) {
    return [
        'first_name' => $faker->name,
        'last_name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'companies_id' => Companies::all()->random()->id,
        'phone' => $faker->phoneNumber,


    ];
});
