@extends('layouts.app')

@section('content')

<div class="container">
<div class="row">
<div class="col-md-12">
<h1>
    Employees
    <!-- .float-right>a.btn.btn-primary{New employees} -->

</h1>
<div class="card">
    <div class="card-header">employees
        <div class="float-right"><a href="{{ route('employeesadd')}}" class="btn btn-primary">New Employees</a></div>
    </div>
    <div class="card-body">
    <!-- table.table>thead>tr>th{ID}+th{Title}+th{Created At} -->
    <table class="table">
        <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Company</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>

        @foreach ($employees as $employee)
        <tr>
            <td>{{ ++$i}}</td>
            <td>{{ $employee->first_name}}</td>
            <td>{{ $employee->companies->name}}</td>
            <td>{{ $employee->email}}</td>
            <td>{{ $employee->phone}}</td>
            <!-- a.btn.btn-success{Edit} -->
            <td>
            <!-- cara dptkan id boleh blako nk tulis employees or employees->id -->
            <a href=" {{ route('employeesedit', $employee) }}" class="btn btn-primary">Edit</a>
           <a href=" {{ route('employeesdelete', $employee->id) }}" class="btn btn-danger" onclick="return confirm('Are you sure?')">Delete</a>
            </td>
        </tr>
        @endforeach

        </tbody>
    </table>
    {{ $employees->links()}}
    </div>
</div>

</div>

</div>

</div>

@endsection
