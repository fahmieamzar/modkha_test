<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'admin',
                'password' => '$2y$10$IBSjh3ZGIR99GTf.SVN.g.kKYBhoH6HNTdZ76LPYz5A5kU6MDj9G.',
                'email' => 'admin@admin.com',

            ),
        ));
    }
}
