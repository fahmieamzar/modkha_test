<form action="{{ $action}}" method="POST">
    @csrf
    <!-- .form-group>label{title}+input.form-control[name=title] -->

        <div class="form-group"><label for="">First Name</label>
        <input type="text" class="form-control {{ $errors->has('first_name')? 'is-invalid': ''}}" name="first_name" value="{{  isset($employees) ? $employees->first_name : '' }}">
        @if($errors->has('name'))
        <div class="invalid-feedback">{{ $errors->first('name')}}</div>
        @endif
        </div>

        <div class="form-group"><label for="">Last Name</label>
        <input type="last_name" class="form-control {{ $errors->has('last_name')? 'is-invalid': ''}}" name="last_name" value="{{  isset($employees) ? $employees->last_name : '' }}">
        @if($errors->has('last_name'))
        <div class="invalid-feedback">{{ $errors->first('last_name')}}</div>
        @endif
        </div>

        <div class="form-group"><label for="">Email</label>
        <input type="email" class="form-control {{ $errors->has('email')? 'is-invalid': ''}}" name="email" value="{{  isset($employees) ? $employees->email : '' }}">
        @if($errors->has('email'))
        <div class="invalid-feedback">{{ $errors->first('email')}}</div>
        @endif
        </div>

        <div class="form-group"><label for="">Phone</label>
        <input type="text" class="form-control {{ $errors->has('phone')? 'is-invalid': ''}}" name="phone" value="{{  isset($employees) ? $employees->phone : '' }}">
        @if($errors->has('phone'))
        <div class="invalid-feedback">{{ $errors->first('phone')}}</div>
        @endif
        </div>

        <!-- .form-group>label{Body}+textarea.form-control[name=body] -->

        <div class="form-group"><label for="">Company</label>
            <select class="form-control" name="companies_id">
                @foreach ($company as $employee)
                <option value="{{$employee->companies_id}}" @if(isset($employees) && $employees->companies_id == $employee->companies_id) selected @endif>{{$employee->companies->name}}</option>
                @endforeach
              </select>
              @error('employees')
              <div class="invalid-feedback">{{ $errors->first('employees')}}</div>
          @enderror
        </div>

        <!-- button.btn.btn-primary{submit} -->

        <button class="btn btn-primary">submit</button>

    </form>
