<?php

namespace App;
use App\Employees;
use Illuminate\Database\Eloquent\Model;

class Companies extends Model
{

    protected $table = 'companies';

    protected $fillable = [
        'id', 'name', 'email', 'logo', 'address', 'website',
    ];

    public function employees() //nama function kne sma dgn model n plural
    {
        // return $this->hasMany('App/Article');

        return $this->hasMany(Employees::class, 'id', 'companies_id');
    }
}
