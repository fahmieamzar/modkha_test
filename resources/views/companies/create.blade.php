@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1>New Companies</h1>
            @include('companies.form', ['action' => route('store')])
        </div>
    </div>
</div>
@endsection
