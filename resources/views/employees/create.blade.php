@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1>New Employees</h1>
            @include('employees.form', ['action' => route('employeesstore')])
        </div>
    </div>
</div>
@endsection
