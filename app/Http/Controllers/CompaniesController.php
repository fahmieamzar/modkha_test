<?php

namespace App\Http\Controllers;

use App\Companies;
use Illuminate\Http\Request;

class CompaniesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = Companies::latest()->paginate(5);

        return view('companies.index',compact('companies'))
            ->with('i', (request()->input('page', 1) - 1) * 5);


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('companies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);
// dd($request->hasfile('logo'));
        if ($files = $request->file('logo')) {
            $destinationPath = 'storage'; // upload path
            $profileImage = date('YmdHis') . "." . $files->getClientOriginalExtension();
            $files->move($destinationPath, $profileImage);
            $companies['logo'] = $destinationPath.'/'.$profileImage;
         }
//dd($companies['logo']);
        request()->logo =$companies['logo'];
        $companies = new companies;
        $companies->name = request()->name;
        $companies->email = request()->email;
        $companies->website = request()->website;
        $companies->address = request()->address;
        $companies->logo = request()->logo;
        $companies->save();

        return redirect()->route('index')
        ->with('success','companies updated successfully');


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Companies  $companies
     * @return \Illuminate\Http\Response
     */
    public function show(Companies $companies)
    {
        return view('companies.show',compact('companies'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Companies  $companies
     * @return \Illuminate\Http\Response
     */
    public function edit(Companies $companies)
    {
       // dd($companies);
       return view('companies.edit',compact('companies'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Companies  $companies
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Companies $companies)
    {
        $request->validate([
            'name' => 'required',
        ]);
//dd($request);
        if ($files = $request->file('logo')) {
            $destinationPath = 'storage'; // upload path
            $profileImage = date('YmdHis') . "." . $files->getClientOriginalExtension();
            $files->move($destinationPath, $profileImage);
            $companies['logo'] = $destinationPath.'/'.$profileImage;
         }
        request()->logo = $companies['logo'];

        $companies->update([
            $companies->name = request()->name,
            $companies->email = request()->email,
            $companies->website = request()->website,
            $companies->address = request()->address,
            $companies->logo = request()->logo
        ]);

        return redirect()->route('index')
        ->with('success','companies updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Companies  $companies
     * @return \Illuminate\Http\Response
     */
    public function destroy(Companies $companies)
    {
        //dd($companies);
        $companies->delete();

        return redirect()->route('index')
        ->with('success','companies deleted successfully');
    }
}
