<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Companies;

class Employees extends Model
{
    protected $table = 'employees';

    protected $fillable = [
        'id', 'first_name', 'last_name', 'email', 'companies_id', 'phone',
    ];

    public function companies()
    {
        return $this->belongsTo(Companies::class, 'companies_id', 'id');
    }
}
