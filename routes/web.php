<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
//Route::resource('companies','CompaniesController');

Route::get('/companies', 'CompaniesController@index')->name('index');


Route::get('/add', 'CompaniesController@create')->name('add');

Route::post('/store', 'CompaniesController@store')->name('store');


Route::get('/edit/{companies}', 'CompaniesController@edit')->name('edit');

Route::post('/update/{companies}', 'CompaniesController@update')->name('update');


Route::get('/delete/{companies}', 'CompaniesController@destroy')->name('delete');


Route::get('/employees', 'EmployeesController@index')->name('employees');


Route::get('/employees/add', 'EmployeesController@create')->name('employeesadd');

Route::post('/employees/store', 'EmployeesController@store')->name('employeesstore');


Route::get('/employees/edit/{employees}', 'EmployeesController@edit')->name('employeesedit');

Route::post('/employees/update/{employees}', 'EmployeesController@update')->name('employeesupdate');


Route::get('/employees/delete/{employees}', 'EmployeesController@destroy')->name('employeesdelete');
