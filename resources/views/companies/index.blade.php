@extends('layouts.app')

@section('content')

<div class="container">
<div class="row">
<div class="col-md-12">
<h1>
    Companiess
    <!-- .float-right>a.btn.btn-primary{New companies} -->
</h1>
<div class="card">
    <div class="card-header">Companiess
        <div class="float-right"><a href="{{ route('add')}}" class="btn btn-primary">New companies</a></div>
    </div>
    <div class="card-body">
    <!-- table.table>thead>tr>th{ID}+th{Title}+th{Created At} -->
    <table class="table">
        <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Address</th>
                <th>Website</th>
                <th>Email</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($companies as $companie)
        <tr>
            <td>{{ ++$i}}</td>
            <td>{{ $companie->name}}</td>
            <td>{{ $companie->address}}</td>
            <td>{{ $companie->website}}</td>
            <td>{{ $companie->email}}</td>
            <!-- a.btn.btn-success{Edit} -->
            <td>
            <!-- cara dptkan id boleh blako nk tulis companies or companies->id -->
            <a href=" {{ route('edit', $companie) }}" class="btn btn-primary">Edit</a>
           <a href=" {{ route('delete', $companie->id) }}" class="btn btn-danger" onclick="return confirm('Are you sure?')">Delete</a>
            </td>
        </tr>
        @endforeach

        </tbody>
    </table>
    {{ $companies->links()}}
    </div>
</div>

</div>

</div>

</div>

@endsection
