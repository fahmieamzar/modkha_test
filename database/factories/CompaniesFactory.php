<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(App\Companies::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'logo' => $faker->image('storage/app/public',100,100, null, false),
        'website' => $faker->domainName,
        'address' => $faker->address,
    ];
});
